<?php

namespace App\Http\Controllers;

use App\Mail\Send;
use App\Models\User;
use App\Transformers\UserTransformer;
use Dingo\Api\Routing\Helpers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class IndexController extends Controller
{
    use Helpers;
    public function index()
    {
//        return User::all();
//        $user = User::find(1);
//       return $this->response->array($user->toArray());
//        $user = User::all();
//        return $this->response->collection($user,new UserTransformer());
//        Mail::to('gyc_max@163.com')->queue(new Send('gyc_max@163.com'));
//        $collects = collect(['taylor', 'abigail', null])->map(function ($item){
//            return strtoupper($item);
//        })->reject(function ($item){
//            return empty($item);
//        });
        $collection = collect([
            [
                'user_id' => '1',
                'title' => 'Helpers in Laravel',
                'content' => 'Create custom helpers in Laravel',
                'category' => 'php'
            ],
            [
                'user_id' => '2',
                'title' => 'Testing in Laravel',
                'content' => 'Testing File Uploads in Laravel',
                'category' => 'php'
            ],
            [
                'user_id' => '3',
                'title' => 'Telegram Bot',
                'content' => 'Crypto Telegram Bot in Laravel',
                'category' => 'php'
            ],
        ]);
        $filter = $collection->filter(function ($item,$key){
            if ($item['user_id'] == 2){
                return true;
            }
        });
        
        $first = $collection->first(function ($item,$key){
            return true;
        });

        return $filter->all();
    }
}
