<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Transformers\UserTransformer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AuthController extends BaseController
{
    /**
     * @Desc 获取用户信息
     * @User Aiden
     * @DateTime: 05-24-0024 17:59
     * @Return \Dingo\Api\Http\Response
     */
    public function index()
    {
        $users = Auth::user();
        return $this->response()->item($users,new UserTransformer());
    }

    /**
     * @Desc 注册
     * @User Aiden
     * @DateTime: 05-24-0024 18:31
     * @param Request $request
     * @Return \Dingo\Api\Http\Response
     */
    public function register(Request $request)
    {
        $rules = [
            'name' => ['required'],
            'email' => ['required'],
            'password' => ['required', 'min:6', 'max:16'],
        ];
        $payload = $request->only('name', 'email', 'password');
        $validator = Validator::make($payload, $rules);
        // 验证格式
        if ($validator->fails()) {
            return $this->response->array(['error' => $validator->errors()]);
        }
        // 创建用户
        $result = User::create([
            'name' => $payload['name'],
            'email' => $payload['email'],
            'password' => bcrypt($payload['password']),
        ]);
        if ($result) {
            return $this->response->array(['success' => '创建用户成功']);
        } else {
            return $this->response->array(['error' => '创建用户失败']);
        }
    }

    /**
     * @Desc 登录
     * @User Aiden
     * @DateTime: 05-24-0024 18:16
     * @param Request $request
     * @Return \Dingo\Api\Http\Response|void
     */
    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');
        if ($token = $this->guard()->attempt($credentials)) {
            return $this->respondWithToken($token);
        }
        return $this->response->errorUnauthorized('登录失败');
    }

    /**
     * @Desc 退出
     * @User Aiden
     * @DateTime: 05-24-0024 18:35
     * @Return \Dingo\Api\Http\Response
     */
    public function logout()
    {
        $this->guard()->logout();
        return $this->response->array(['message' => '退出成功']);
    }

    /**
     * @Desc 刷新Token
     * @User Aiden
     * @DateTime: 05-24-0024 18:35
     * @Return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken($this->guard()->refresh());
    }

    /**
     * @Desc 格式化返回token
     * @User Aiden
     * @DateTime: 05-24-0024 18:36
     * @param $token
     * @Return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => $this->guard()->factory()->getTTL() * 60
        ]);
    }

    /**
     * @Desc 看守器
     * @User Aiden
     * @DateTime: 05-24-0024 18:36
     * @Return \Illuminate\Contracts\Auth\Guard|\Illuminate\Contracts\Auth\StatefulGuard
     */
    public function guard()
    {
        return Auth::guard('api');
    }
}
