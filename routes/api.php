<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
$api = app('Dingo\Api\Routing\Router');

$api->version('v1',function ($api){
    $api->get('index',[\App\Http\Controllers\IndexController::class,'index']);
    $api->post('user/login',[\App\Http\Controllers\AuthController::class,'login'])->name('auth.login');
    $api->post('user/register',[\App\Http\Controllers\AuthController::class,'register'])->name('auth.register');
    $api->group(['middleware'=>'api.auth'],function($api){
        $api->get('user/index',[\App\Http\Controllers\AuthController::class,'index']);
        $api->get('user/logout',[\App\Http\Controllers\AuthController::class,'logout']);
        $api->get('user/refresh',[\App\Http\Controllers\AuthController::class,'refresh']);
    });
});
