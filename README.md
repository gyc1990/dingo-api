### 安装JWT
```
composer require tymon/jwt-auth
```
### 发布配置 执行此命令会在 app/config 下生成 jwt.php 的配置文件。
```
php artisan vendor:publish --provider="Tymon\JWTAuth\Providers\LaravelServiceProvider"
```
### 生成密钥 
##### 并且可以在.env 文件中看到生成了一个配置项：
```
php artisan jwt:secret
```
### 更新User模型 模型为 app/Models\User.php，
### 修改 / 配置 auth.php
```
'guards' => [
        'web' => [
            'driver' => 'session',
            'provider' => 'users',
        ],
        'api' => [
            'driver' => 'jwt', //把token改为jwt
            'provider' => 'users',//对应的provider
        ],
    ],
    'providers' => [
        'users' => [
        'driver' => 'eloquent',
        'model' => App\Models\User::class,
        ],
    ],
```
### 安装 dingo-api
```
composer require dingo/api
```
### 执行
```
php artisan vendor:publish --provider="Dingo\Api\Provider\LaravelServiceProvider"
```
### 配置信息
```
执行成功后生成一个位于 app/config 下的配置文件 api.php。
配置.env 文件：
API_STANDARDS_TREE=x
API_PREFIX=api
API_VERSION=v1
API_CONDITIONAL_REQUEST=false
API_STRICT=false
API_DEFAULT_FORMAT=json
API_DEBUG=false
```

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
